export let chunkedIndexFilt = { plugin: "peek_plugin_chunked_index" };
export let chunkedIndexTuplePrefix = "peek_plugin_chunked_index.";

export let chunkedIndexObservableName = "peek_plugin_chunked_index";
export let chunkedIndexActionProcessorName = "peek_plugin_chunked_index";
export let chunkedIndexTupleOfflineServiceName = "peek_plugin_chunked_index";

export let chunkedIndexBaseUrl = "peek_plugin_chunked_index";

export let chunkedIndexCacheStorageName = "peek_plugin_chunked_index.cache";
